import React, { createContext, useState } from 'react'
import { IGame } from '../features/game/domain/entities/GameType'

export type GameContextTypee = {
  game: IGame | undefined
  updateGame: (game: IGame) => void
}

type GameContextType = [{ game?: IGame }, { updateGame: (game: IGame) => void }]

export const GameContext = createContext<GameContextType | null>(null)

export const GameProvider: React.FC<React.ReactNode> = (props) => {
  const [game, setGame] = useState<IGame | undefined>()

  const handleUpdateGame = (data: IGame) => {
    if (!data) setGame(undefined)

    setGame(data)
  }

  return (
    <GameContext.Provider value={[{ game }, { updateGame: (data: IGame) => handleUpdateGame(data) }]}>
      {props.children}
    </GameContext.Provider>
  )
}
