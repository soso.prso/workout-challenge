import { FC, useState } from 'react'
import { View, Button, Text, StyleSheet, TextInput, ScrollView, TouchableOpacity } from 'react-native'

import { Game } from '../../../domain/entities/Game'
import Title from '../../../../../components/Title'
import { IPlayer } from '../../../domain/entities/PlayerType'
import { IGame } from '../../../domain/entities/GameType'

type StartGameScreenProps = {
  onStartNewGame: (game: IGame) => void
}

const StartGameScreen: FC<StartGameScreenProps> = ({ onStartNewGame }) => {
  const [inputs, setInputs] = useState<IPlayer[]>([{ id: 1, name: '', hasFailed: false, counter: 0 }])

  const addHandler = () => {
    const _inputs = [...inputs]
    _inputs.push({ id: '', name: '', hasFailed: false, counter: 0 })
    setInputs(_inputs)
  }

  const deleteHandler = (id: number) => {
    const _inputs = inputs.filter((input, index) => index != id)
    setInputs(_inputs)
  }

  const inputHandler = (text: string, id: number) => {
    const _inputs = [...inputs]
    _inputs[id].name = text
    _inputs[id].id = id + 1
    setInputs(_inputs)
  }

  const startGameHandler = () => {
    const game = new Game(inputs)

    onStartNewGame(game)
  }

  return (
    <View style={styles.screen}>
      <Title>{inputs.length} joueurs</Title>

      <ScrollView style={styles.inputsContainer}>
        {inputs.map((input, key) => (
          <View style={styles.inputContainer} key={key}>
            <TextInput
              style={styles.textInput}
              placeholder={'Enter Name'}
              value={input.name}
              onChangeText={(text) => inputHandler(text, key)}
            />
            <TouchableOpacity onPress={() => deleteHandler(key)}>
              <Text style={{ color: 'red', fontSize: 13 }}>Delete</Text>
            </TouchableOpacity>
          </View>
        ))}
      </ScrollView>

      <Button title="Add" onPress={addHandler} />

      <Button title="Go" onPress={startGameHandler} disabled={inputs.length < 2} />
    </View>
  )
}

export default StartGameScreen

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 20,
    backgroundColor: 'white',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 20,
  },
  inputsContainer: {
    flex: 1,
    marginBottom: 20,
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',
  },
  textInput: {
    // flex: 1,
    height: 70,
    fontSize: 32,
  },
})
