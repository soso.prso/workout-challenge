import { FC, useContext } from 'react'
import { View, Text, FlatList, Button, StyleSheet } from 'react-native'
import Title from '../../../../../components/Title'

import { GameContext } from '../../../../../contexts/GameContext'

type RankingScreenProps = {
  onFinishGame: () => void
}

const RankingScreen: FC<RankingScreenProps> = ({ onFinishGame }) => {
  const [state, actions] = useContext(GameContext)
  const { game } = state
  const { updateGame } = actions

  const { ranking } = game

  console.log('FINAL RANKING', ranking)

  const winner = ranking[0]
  return (
    <View style={styles.screen}>
      <Title>Ranking</Title>

      <Text>Winner : {winner.name}</Text>

      <FlatList
        data={ranking}
        renderItem={(itemData) => (
          <View style={styles.playerItem}>
            <Text style={styles.playerText}>{itemData.item.name}</Text>
            <Text style={styles.playerText}>{itemData.item.counter}</Text>
          </View>
        )}
        keyExtractor={(item) => item.id}
      />

      <Button title="New Game" onPress={onFinishGame} />
    </View>
  )
}

export default RankingScreen

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  playerItem: {
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: 'brown',
    borderWidth: 1,
    padding: 10,
  },
  playerText: {
    fontSize: 24,
  },
})
