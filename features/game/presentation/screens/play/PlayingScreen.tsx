import { FC, useContext } from 'react'
import { View, StyleSheet, Button } from 'react-native'

import Title from '../../../../../components/Title'
import { GameContext } from '../../../../../contexts/GameContext'
import { IGame } from '../../../domain/entities/GameType'
import DisplayedCard from '../../components/DisplayedCard'
import PlayersList from '../../components/PlayersList'

const PlayingScreen: FC<IGame> = ({
  playersInit,
  playersPlaying,
  currentIndexPlayer,
  displayedCard,
  drawCard,
  giveUp,
}) => {
  const [state, actions] = useContext(GameContext)
  const { updateGame } = actions

  const handleDrawCard = () => {
    const newGame = drawCard()
    updateGame(newGame)
  }

  const handleGiveUp = () => {
    const newGame = giveUp()
    updateGame(newGame)
  }

  return (
    <View style={styles.screen}>
      <Title>Lets go</Title>
      <PlayersList playersInit={playersInit} playersPlaying={playersPlaying} currentIndexPlayer={currentIndexPlayer} />

      {displayedCard && <DisplayedCard displayedCard={displayedCard} />}

      <View style={styles.ctaContainer}>
        <Button title="Abandonner" onPress={handleGiveUp} />
        <Button title="Piocher" onPress={handleDrawCard} />
      </View>
    </View>
  )
}

export default PlayingScreen

const styles = StyleSheet.create({
  screen: {
    backgroundColor: 'brown',
    flex: 1,
    alignItems: 'center',
  },
  ctaContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  cta: {
    marginHorizontal: 20,
  },
})
