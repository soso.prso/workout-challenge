import { FC, useContext, useEffect, useState } from 'react'

import PlayingScreen from './PlayingScreen'
import StartGameScreen from './StartGameScreen'
import { GameContext } from '../../../../../contexts/GameContext'
import RankingScreen from './RankingScreen'
import { IGame } from '../../../domain/entities/GameType'
import { Text } from 'react-native'

const GameContainer: FC = () => {
  const [state, actions] = useContext(GameContext)
  const { game } = state
  const { updateGame } = actions

  const startNewGameHandler = (game: IGame) => {
    updateGame(game)
  }

  const finishGameHandler = () => {
    updateGame()
  }

  if (!game) return <StartGameScreen onStartNewGame={startNewGameHandler} />

  if (game.isStarted && !game.isFinished) return <PlayingScreen {...game} />

  if (game.isFinished) return <RankingScreen onFinishGame={finishGameHandler} />

  return <Text>Loading</Text>
}

export default GameContainer

// if (game.isStarted && !game.isFinished) return <PlayingScreen {...game} />
