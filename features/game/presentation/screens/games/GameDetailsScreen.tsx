import { NativeStackScreenProps } from '@react-navigation/native-stack'
import React from 'react'
import { View, Text } from 'react-native'

type GameDetailsScreenProps = NativeStackScreenProps<any, 'GamesList'>

const GameDetailsScreen: React.FC<GameDetailsScreenProps> = ({ route }) => {
  console.log('route.params', route.params)
  const id = route.params.id
  const winnerName = route.params.winner.name
  const winnerCounter = route.params.winner.counter

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>{winnerName}</Text>
      <Text>{winnerCounter}</Text>
      <Text>Date</Text>
    </View>
  )
}

export default GameDetailsScreen
