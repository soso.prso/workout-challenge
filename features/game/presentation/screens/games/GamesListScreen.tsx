import { NativeStackScreenProps } from '@react-navigation/native-stack'
import React from 'react'
import { View, Text, Button, FlatList, Pressable, Platform, StyleSheet } from 'react-native'
import { dataGames } from '../../../data/games'

type GamesListScreenProps = NativeStackScreenProps<any, 'GamesList'>

const GamesListScreen: React.FC<GamesListScreenProps> = ({ navigation }) => {
  function renderCategoryItem(itemData) {
    const winner = itemData.item.ranking[0]

    function pressHandler() {
      navigation.navigate('GameDetails', {
        id: itemData.item.id,
        winner: winner,
      })
    }

    return (
      <View style={styles.gridItem}>
        <Pressable
          android_ripple={{ color: '#ccc' }}
          style={({ pressed }) => [styles.button, pressed ? styles.buttonPressed : null]}
          onPress={pressHandler}
        >
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 16,
            }}
          >
            <View>
              <Text>{winner.name}</Text>
              <Text>{winner.counter}</Text>
            </View>

            <Text>12/09/2022</Text>
          </View>
        </Pressable>
      </View>
    )
  }

  return (
    <View style={{ flex: 1, backgroundColor: 'orange' }}>
      <FlatList data={dataGames} keyExtractor={(item) => item.id} renderItem={renderCategoryItem} />
    </View>
  )
}

export default GamesListScreen

const styles = StyleSheet.create({
  gridItem: {
    flex: 1,
    height: 100,
    elevation: 4,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  button: {
    flex: 1,
  },
  buttonPressed: {
    opacity: 0.5,
  },
})
