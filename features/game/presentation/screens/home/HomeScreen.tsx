import { useNavigation } from '@react-navigation/native'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import React from 'react'
import { View, Text, Button } from 'react-native'

type HomeScreenProps = NativeStackScreenProps<any, 'Home'>

const HomeScreen: React.FC<HomeScreenProps> = ({ navigation }) => {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
      <Button title="Play" onPress={() => navigation.navigate('Play')} />
    </View>
  )
}

export default HomeScreen
