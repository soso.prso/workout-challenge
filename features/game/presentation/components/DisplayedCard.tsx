import { FC } from 'react'
import { View, Text, StyleSheet } from 'react-native'

type DisplayedCardProps = {
  displayedCard: string
}

const DisplayedCard: FC<DisplayedCardProps> = ({ displayedCard }) => {
  return (
    <View style={styles.displayedCardWrapper}>
      <Text style={styles.displayedCard}>{displayedCard.toString()}</Text>
    </View>
  )
}

export default DisplayedCard

const styles = StyleSheet.create({
  displayedCard: {
    fontSize: 75,
    fontWeight: 'bold',
  },
  displayedCardWrapper: {
    flexDirection: 'column',
    borderColor: 'black',
    backgroundColor: '#7189e1',
    borderWidth: 2,
    borderRadius: 20,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    height: 200,
    width: 150,
    marginVertical: 20,
  },
})
