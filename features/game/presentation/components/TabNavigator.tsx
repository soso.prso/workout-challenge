import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Ionicons } from '@expo/vector-icons'

import GameContainer from '../screens/play/GameContainer'
import HomeScreen from '../screens/home/HomeScreen'
import { useContext } from 'react'
import { GameContext } from '../../../../contexts/GameContext'
import GamesListScreen from '../screens/games/GamesListScreen'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import GameDetailsScreen from '../screens/games/GameDetailsScreen'
import { getFocusedRouteNameFromRoute } from '@react-navigation/native'

const BottomTab = createBottomTabNavigator()
const Stack = createNativeStackNavigator()

const GamesListStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="GamesList" component={GamesListScreen} options={{ headerShown: false }} />
      <Stack.Screen name="GameDetails" component={GameDetailsScreen} options={{ title: 'Détails' }} />
    </Stack.Navigator>
  )
}

const TabNavigator = () => {
  const [state] = useContext(GameContext)
  const { game } = state

  return (
    <BottomTab.Navigator screenOptions={{ tabBarShowLabel: false }}>
      <BottomTab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: 'Accueil',
          tabBarIcon: ({ color, size }) => <Ionicons name="home" color={color} size={size} />,
        }}
      />
      <BottomTab.Screen
        name="Play"
        component={GameContainer}
        options={{
          title: 'Menu Jeu',
          tabBarStyle: { display: game && game.isStarted && !game.isFinished ? 'none' : 'flex' },
          tabBarIcon: ({ color, size }) => <Ionicons name="play" color={color} size={size} />,
        }}
      />
      <BottomTab.Screen
        name="GamesListStack"
        component={GamesListStack}
        options={({ route }) => ({
          title: 'Mes parties',
          headerShown: getHeaderVisibility(route),
          tabBarIcon: ({ color, size }) => <Ionicons name="list" color={color} size={size} />,
        })}
      />
    </BottomTab.Navigator>
  )
}

export default TabNavigator

const getHeaderVisibility = (route) => {
  const routeName = getFocusedRouteNameFromRoute(route) ?? 'Home'
  console.log(routeName)

  if (routeName === 'GameDetails') {
    return false
  }

  return true
}
