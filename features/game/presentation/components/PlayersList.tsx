import { FC } from 'react'
import { FlatList, Text, View, StyleSheet } from 'react-native'
import { IPlayer } from '../../domain/entities/PlayerType'

type PlayersListProps = {
  playersInit: IPlayer[]
  playersPlaying: IPlayer[]
  currentIndexPlayer: number
}

const PlayersList: FC<PlayersListProps> = ({ playersInit, playersPlaying, currentIndexPlayer }) => {
  return (
    <View style={styles.playersContainer}>
      <FlatList
        data={playersPlaying}
        renderItem={(itemData) => (
          <View style={[styles.playerWrapper, itemData.index === currentIndexPlayer && styles.currentPlayerWrapper]}>
            <Text style={[styles.playerText, itemData.index === currentIndexPlayer && styles.currentPlayer]}>
              {itemData.item.name}
            </Text>
            <Text>{itemData.item.counter}</Text>
          </View>
        )}
        keyExtractor={(item) => item.name}
      />
    </View>
  )
}

export default PlayersList

const styles = StyleSheet.create({
  playersContainer: {
    flexDirection: 'row',
  },
  playerWrapper: {
    backgroundColor: 'white',
    borderColor: 'brown',
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  currentPlayerWrapper: {
    backgroundColor: '#6efe7a',
  },
  hasFailedPlayerWrapper: {
    backgroundColor: '#ff6060',
  },
  playerText: {
    fontSize: 24,
  },
  currentPlayer: {
    fontWeight: 'bold',
  },
  hasFailedPlayer: {
    color: 'grey',
  },
})
