import { fourPlayers } from './players'

export const dataGames = [
  {
    id: 0,
    playersInit: fourPlayers,
    playersPlaying: fourPlayers,
    ranking: fourPlayers,
  },
  {
    id: 1,
    playersInit: fourPlayers,
    playersPlaying: fourPlayers,
    ranking: fourPlayers,
  },
  {
    id: 22,
    playersInit: fourPlayers,
    playersPlaying: fourPlayers,
    ranking: fourPlayers,
  },
]
