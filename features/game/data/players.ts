import { IPlayer } from '../domain/entities/PlayerType'

export const twoPlayers: IPlayer[] = [
  {
    id: 1,
    name: 'so',
    counter: 0,
    hasFailed: false,
  },
  {
    id: 2,
    name: 'mlk',
    counter: 0,
    hasFailed: false,
  },
]

export const threePlayers: IPlayer[] = [
  {
    id: 1,
    name: 'so',
    counter: 0,
    hasFailed: false,
  },
  {
    id: 2,
    name: 'mlk',
    counter: 0,
    hasFailed: false,
  },
  {
    id: 3,
    name: 'hana',
    counter: 0,
    hasFailed: false,
  },
]

export const fourPlayers: IPlayer[] = [
  {
    id: 1,
    name: 'So',
    counter: 89,
    hasFailed: false,
  },
  {
    id: 2,
    name: 'Malika',
    counter: 0,
    hasFailed: false,
  },
  {
    id: 3,
    name: 'Hana',
    counter: 0,
    hasFailed: false,
  },
  {
    id: 4,
    name: 'Geo',
    counter: 0,
    hasFailed: false,
  },
]

export const getRanking = (players: IPlayer[], winner?: IPlayer) => {
  if (winner) {
    return [...players, winner].reverse()
  }

  /*
  return players.sort(function compare(a, b) {
    if (a.counter < b.counter) return -1
    if (a.counter > b.counter) return 1
    return 0
  })
  */

  return players
    .sort(function (a, b) {
      return a.counter - b.counter
    })
    .reverse()
}
