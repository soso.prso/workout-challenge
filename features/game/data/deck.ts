import { IDeck } from '../domain/entities/DeckType'

export const DECK_TYPE: IDeck = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'R', 'S']

export const buildDeck = (arr: IDeck, n: number): IDeck => {
  let newArr: IDeck = []
  while (n >= 1) {
    newArr = [...newArr, ...arr]
    n--
  }
  return newArr
}
