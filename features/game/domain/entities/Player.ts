import { IPlayer } from './PlayerType'

export class PlayerClass implements IPlayer {
  id: number
  name: string
  hasFailed: boolean
  counter: number

  constructor(id: number, name: string, hasFailed: boolean, counter: number) {
    this.id = id
    this.name = name
    this.hasFailed = hasFailed || false
    this.counter = counter || 0
  }
}
