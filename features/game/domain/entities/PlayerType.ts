export type IPlayer = {
  id: number
  name: string
  counter: number
  hasFailed: boolean
}
