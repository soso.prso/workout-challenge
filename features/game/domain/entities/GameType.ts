import { IDeck } from './DeckType'
import { IPlayer } from './PlayerType'

export type IGame = {
  playersInit: IPlayer[]
  playersPlaying: IPlayer[]
  ranking: IPlayer[]
  currentIndexPlayer: number
  deck: IDeck
  displayedCard: string | number
  isClockwise: boolean
  isStarted: boolean
  isFinished: boolean
  drawCard: () => void
  giveUp: () => void
  winner: IPlayer[] | IPlayer
}
