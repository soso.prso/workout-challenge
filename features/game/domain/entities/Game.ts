import { buildDeck, DECK_TYPE } from '../../data/deck'
import { getRanking } from '../../data/players'
import { ICard, IDeck } from './DeckType'
import { IGame } from './GameType'
import { PlayerClass } from './Player'
import { IPlayer } from './PlayerType'

export class Game implements IGame {
  playersInit: IPlayer[]
  playersPlaying: IPlayer[]
  ranking: IPlayer[]
  currentIndexPlayer: number
  numberOfPlayers: number
  deck: IDeck
  displayedCard: string | number
  isClockwise: boolean
  isStarted: boolean
  isFinished: boolean
  winner: IPlayer[] | IPlayer

  constructor(players: IPlayer[]) {
    this.playersInit = players
    this.playersPlaying = players
    this.ranking = []
    this.currentIndexPlayer = 0
    this.numberOfPlayers = players.length
    this.deck = buildDeck(DECK_TYPE, 2)
    this.displayedCard = '?'
    this.isClockwise = true
    this.isStarted = true
    this.isFinished = false
    this.winner = []
  }

  private changePlayer = (): void => {
    if (this.isClockwise) {
      if (this.currentIndexPlayer >= this.playersPlaying.length - 1) {
        this.currentIndexPlayer = 0
      } else {
        this.currentIndexPlayer = this.currentIndexPlayer + 1
      }
    } else {
      if (this.currentIndexPlayer === 0) {
        this.currentIndexPlayer = this.playersPlaying.length - 1
      } else {
        this.currentIndexPlayer = this.currentIndexPlayer - 1
      }
    }
  }

  finish = (isDeckFinished = false): void => {
    if (isDeckFinished) {
      /*
      const winnerX = { ...this.playersPlaying[0] }
      const { id, name, hasFailed, counter } = winnerX
      this.winner = new PlayerClass(id, name, hasFailed, counter)
      */
      const winner = { ...this.playersPlaying[0] }

      this.winner = winner
      this.ranking = getRanking(this.ranking, winner)
      this.playersPlaying = []
    } else {
      const winner = [...this.playersPlaying]
      this.winner = winner

      const rankingWinners = [...this.playersPlaying]
        .sort(function (a, b) {
          return a.counter - b.counter
        })
        .reverse()

      const loosers = [...this.ranking]
      const theRealWinners = [...rankingWinners, ...loosers]
      this.ranking = theRealWinners

      // this.ranking = getRanking([...this.playersPlaying, ...this.ranking])
    }

    this.isFinished = true
  }

  private unstack = (deck: IDeck): [IDeck, ICard] => {
    const cloneDeck = [...deck]

    const cardDrawed = cloneDeck.splice(Math.floor(Math.random() * cloneDeck.length), 1)[0]

    return [cloneDeck, cardDrawed]
  }

  drawCard = (): IGame => {
    /*
    const cloneDeck = [...this.deck]
    // Prévoir le cas ou le deck est terminé
    const cardDrawed = cloneDeck.splice(Math.floor(Math.random() * cloneDeck.length), 1)[0]

    // L'addition on the counter se fait avant d'avoir valider le mouv
    if (typeof cardDrawed === 'number') {
      this.playersInit[this.currentIndexPlayer].counter += cardDrawed
    }

    if (cardDrawed === 'S') {
      this.isClockwise = !this.isClockwise
    }

    this.deck = cloneDeck
    this.displayedCard = cardDrawed

    */

    const [newDeck, newCard] = this.unstack(this.deck)

    // L'addition on the counter se fait avant d'avoir valider le mouv
    if (typeof newCard === 'number') {
      this.playersInit[this.currentIndexPlayer].counter += newCard
    }

    if (newCard === 'S') {
      this.isClockwise = !this.isClockwise
    }

    this.deck = newDeck
    this.displayedCard = newCard

    // FIN DE PARTIE
    if (this.deck.length === 0) {
      // this.isFinished = true
      // this.ranking = getRanking([...this.playersPlaying, ...this.ranking])
      this.finish()
    }

    // next player
    this.changePlayer()

    const that = { ...this }

    return that
  }

  giveUp = (): IGame => {
    const player = this.playersPlaying[this.currentIndexPlayer]
    this.ranking = [...this.ranking, player]

    this.playersInit[player.id - 1].hasFailed = true

    this.playersPlaying = this.playersPlaying.filter((item) => item.id !== player.id)

    if (this.playersPlaying.length === 1) {
      /*
      const winnerX = { ...this.playersPlaying[0] }
      const { id, name, hasFailed, counter } = winnerX
      const winner = new PlayerClass(id, name, hasFailed, counter)
      console.log('winner', winner)
      console.log('this.ranking', this.ranking)

      this.ranking = getRanking(this.ranking, winner)
      this.playersPlaying = []
      this.isFinished = true
      console.log('FIN GAME')
      */
      this.finish(true)
    }

    this.changePlayer()

    const that = { ...this }

    return that
  }
}
