export type IDeck = ICard[]

export type ICard = string | number
