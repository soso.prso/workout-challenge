import { NavigationContainer } from '@react-navigation/native'
import { StatusBar } from 'expo-status-bar'
import { StyleSheet, View } from 'react-native'

import { GameProvider } from './contexts/GameContext'
import TabNavigator from './features/game/presentation/components/TabNavigator'

export default function App() {
  return (
    <NavigationContainer>
      <StatusBar style="auto" />
      <GameProvider>
        <View style={styles.container}>
          <TabNavigator />
        </View>
      </GameProvider>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})
